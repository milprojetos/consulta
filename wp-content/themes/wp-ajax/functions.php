<?php
// adiciona o CSS n� p�gina
function theme_style() {
	
	$parent_style = 'parent-style';	
	
	wp_enqueue_style($parent_style,get_template_directory_uri().'/style.css');
	wp_enqueue_style(
						'child_style',
						get_template_directory_uri().'/style.css',
						array($parent_style),
						wp_get_theme()->get('Version')
					);
	
	wp_enqueue_style(
						'bootstrap',
						get_stylesheet_directory_uri().'/bootstrap/css/bootstrap.min.css'
					);
	wp_enqueue_script(
						'bootstrap',
						get_stylesheet_directory_uri().'/bootstrap/js/bootstrap.min.js'
					);
}
add_action('wp_enqueue_scripts', 'theme_style');

// adcionando o jquery
function register_jquery() {
	wp_enqueue_script('jquery');
}
add_action('wp_enqueue_scripts', 'register_jquery');

// adiciona o ajaxFilter.js ao tema
function register_ajaxFilter() {
	wp_enqueue_script( 
		'ajaxFilter', 
		get_template_directory_uri() . '/js/ajaxFilter.js', 
		array('jquery') 
	);

	wp_localize_script(
		'ajaxFilter',
		'ajax_object',
		array( 'ajax_url' => admin_url( 'admin-ajax.php' ))
	);
}
add_action( 'wp_enqueue_scripts', 'register_ajaxFilter' );

function register_ajaxOnLoad() {
	wp_enqueue_script( 
		'ajaxOnLoad', 
		get_template_directory_uri() . '/js/ajaxOnLoad.js', 
		array('jquery') 
	);

	wp_localize_script(
		'ajaxOnLoad',
		'ajax_object',
		array( 'ajax_url' => admin_url( 'admin-ajax.php' ))
	);
}
add_action( 'wp_enqueue_scripts', 'register_ajaxOnLoad' );

function instantiate_variables(){
	global $wpdb;
	$table = 'curso';

	$cursos = $wpdb->get_results("SELECT * FROM ".$table);
	echo json_encode($cursos, true);
	wp_die();
};
add_action('wp_ajax_instantiate_variables', 'instantiate_variables');
add_action('wp_ajax_nopriv_instantiate_variables', 'instantiate_variables');

// RECEBE O $POST DA TELA DE CONSULTA PUBLICA
function mostrar_diploma($nome,$cod_controle,$curso){
	global $wpdb;
	$table = 'alunos_matriculados';
	
	$result = $wpdb->
				get_results("SELECT * FROM ".$table." options WHERE "
							.$nome." "
							.$curso." "
							.$cod_controle
							);
	$i = 0;
	foreach($result as $key => $row){
		//CONVERS�O DO FORMATO DA DATA					
		$date[$i]['1'] = new DateTime($row->data_ingresso);
		$row->data_ingresso = $date[$i]['1']->format('d/m/Y');
		
		$date[$i]['2'] = new DateTime($row->data_conclusao);
		$row->data_conclusao = $date[$i]['2']->format('d/m/Y');
		
		$date[$i]['3'] = new DateTime($row->data_exp_diploma);
		$row->data_exp_diploma = $date[$i]['3']->format('d/m/Y');
		
		$date[$i]['4'] = new DateTime($row->data_reg_diploma);
		$row->data_reg_diploma = $date[$i]['4']->format('d/m/Y');
		
		$date[$i]['5'] = new DateTime($row->data_publicacao_dou);
		$row->data_publicacao_dou = $date[$i]['5']->format('d/m/Y');
		//FIM DA CONVERS�O DE DATA
		//TRATAMENTO DE CPF
		$row->cpf = 0;
		$cpf_parcial[$i]['0'] = substr($row->cpf_parcial,0,3);
		$cpf_parcial[$i]['1'] = substr($row->cpf_parcial,3,3);
		$row->cpf_parcial = $cpf_parcial[$i]['0'].'.'.$cpf_parcial[$i]['1'];
		$cpf_parcial[$i]['2'] = $row->cpf_parcial.'.XXX-XX';	
		$row->cpf_parcial = $cpf_parcial[$i]['2'];
		//FIM DO TRATAMENTO DE CPF
		$i++;
	}
	
	echo json_encode($result, true);
	wp_die();
}
add_action('wp_ajax_mostrar_diploma', 'mostrar_diploma');
add_action('wp_ajax_nopriv_mostrar_diploma', 'mostrar_diploma');

function input_data_processing(){
	$free_access = false;
	if(isset($_POST['cod_controle'])&&$_POST['cod_controle']!=''){
		$nome = '';
		$curso = '';
		$cod_controle = 'cod_controle_reg = '.$_POST['cod_controle'];
		$free_access = true;
	}
	elseif(isset($_POST['nome'])&&$_POST['nome']!=''){
		$cod_controle = '';
		$nome = 'nome_completo = "'.$_POST['nome'].'"';
		if(isset($_POST['curso'])&&$_POST['curso']!=''){
			$curso = 'AND id_curso = "'.$_POST['curso'].'"';
		}else{
			$curso = '';
		}
		$free_access = true;
	}elseif(isset($_POST['curso'])&&$_POST['curso']!=''){
		$cod_controle = '';
		$nome = '';
		$curso = 'id_curso = "'.$_POST['curso'].'"';
		$free_access = true;
	}
	
	if($free_access){
		mostrar_diploma($nome,$cod_controle,$curso);
	}else{
		$show = (object) [
			'message' => 'Nenhum Resultado Encontrado'
		];
		echo json_encode($show, true);
		wp_die();
	}
	wp_die();
}
add_action('wp_ajax_input_data_processing', 'input_data_processing');
add_action('wp_ajax_nopriv_input_data_processing', 'input_data_processing');
?>