//habilita o uso do $
jQuery(function($){
	$(document).on('hide.bs.modal','#dynamic_large_modal', function () {
			$('.confirm-button').prop('disabled', 'disabled');
			
			$("#name_in").val('');
			$("#cod_reg_in").val('');
			$('.curso-data-in option[value=""]').attr('selected','selected');
			
			$("#name_in").prop('required',true);
			$("#name_in").prop('readonly',false);
		
			$("#curso_in").prop('required',true);
			$(".curso-data-in").css( "background","#fff" );
		
			$("#cod_reg_in").prop('required',false);	
			$("#cod_reg_in").prop('readonly',true);	
		});
	$(document).ready(function(){
		//Configuração inicial de inputs requeridos
		$("#name_in").prop('required',true);
		$("#name_in").prop('readonly',false);
		
		$("#curso_in").prop('required',true);
		$(".curso-data-in").css( "background","#fff" );
		
		$("#cod_reg_in").prop('required',false);
		$("#cod_reg_in").val('');
		$("#cod_reg_in").prop('readonly',true);
		
		//configuração para desabilitar o botão de submit caso algum do inputs exigidos não esteja preenchidos
		$('input').on('keyup', function() {
			if ($("#student_search").valid()) {
				$('.confirm-button').prop('disabled', false);  
			} else {
				$('.confirm-button').prop('disabled', 'disabled');
			}
			
			if($(this).val()==''){
				$('.confirm-button').prop('disabled', 'disabled');
			}
		});
		
		// códigos AJAX a serem executados quando a página carregar para carregar os cursos
		$.ajax({
			type: 'post',
			dataType: 'json',
			url: ajax_object.ajax_url,
			data: {
				'action': 'instantiate_variables',
			},
			success: function(data){
				$('.curso-data-in').append('<option value=""> Selecione um Curso... </option>');
				for(i = 0; i < data.length; i++){
					$('.curso-data-in').append('<option value="'+
													data[i].id_curso
												+'">'+
													data[i].nome_curso
												+'</option>');
				}
			}
		});
	});
	
	//configuração dinamica de inputs requeridos
	$('#name_in').click(function(){
		$('.confirm-button').prop('disabled', 'disabled');
		
		$("#name_in").prop('required',true);
		$("#name_in").prop('readonly',false);
		
		$("#curso_in").prop('required',true);
		$(".curso-data-in").css( "background","#fff" );
		
		$("#cod_reg_in").prop('required',false);
		$("#cod_reg_in").val('');
		$("#cod_reg_in").prop('readonly',true);
	});
	
	$('#curso_in').click(function(){
		$("#name_in").prop('required',true);
		$("#name_in").prop('readonly',false);
		
		$("#curso_in").prop('required',true);
		$(".curso-data-in").css( "background","#fff" );
		
		$("#cod_reg_in").prop('required',false);
		$("#cod_reg_in").val('');
		$("#cod_reg_in").prop('readonly',true);
	});
	
	$('#cod_reg_in').click(function(){
		$('.confirm-button').prop('disabled', 'disabled');
		
		$("#name_in").prop('required',false);
		$("#name_in").val('');
		$("#name_in").prop('readonly',true);
		
		$(".curso-data-in").prop('required',false);
		$('.curso-data-in option[value=""]').attr('selected','selected');
		$(".curso-data-in").css( "background","#eee" );
		
		$("#cod_reg_in").prop('required',true);
		$("#cod_reg_in").prop('readonly',false);
	});
	
	$('#clear_form').click(function(){
		$('.confirm-button').prop('disabled', 'disabled');
	});
});