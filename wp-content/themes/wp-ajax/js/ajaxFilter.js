jQuery(function($){
	var selected_student = function(data,position) {
		$("#nome").val(data[position].nome_completo);
		$("#cpf").val(data[position].cpf_parcial);
		$("#curso").val(data[position].nome_curso);
		$("#cod_curso").val(data[position].emec_curso);
		$("#ies_exp").val(data[position].nome_exp);
		$("#emec_exp").val(data[position].emec_exp);
		$("#ies_reg").val(data[position].nome_reg);
		$("#emec_reg").val(data[position].emec_reg);
		$("#data_in").val(data[position].data_ingresso);
		$("#data_out").val(data[position].data_conclusao);
		$("#data_exp").val(data[position].data_exp_diploma);
		$("#cod_exp").val(data[position].cod_controle_exp);
		$("#data_reg").val(data[position].data_reg_diploma);
		$("#cod_reg").val(data[position].cod_controle_reg);
		$("#data_dou").val(data[position].data_publicacao_dou);
	}
	
	var fill_modal = function(nom,cur,cod) {
		$.ajax({
			type: 'post',
			dataType: 'json',
			url: ajax_object.ajax_url,
			data: {
				'action': 'input_data_processing',
				'nome' : nom, 
				'curso' : cur, 
				'cod_controle' : cod
			},
			success: function(data){
				if(data.length>1){
					for(i = 0;i<data.length;i++){
						$('.dynamic_table_body').append(
							'<tr class="tr-clickable" id="'+i+'">'+
								'<th scope="row">'+
									data[i].cod_controle_reg
								+'</th>'+
								'<td>'+
									data[i].nome_completo
								+'</td>'+
								'<td>'+
									data[i].cpf_parcial
								+'</td>'+
								'<td>'+
									data[i].nome_curso
								+'</td>'+
							'</tr>'
						);
					}
					$('.dynamic-table-aluno').show("fast");
					
					$('.tr-clickable').click(function(){
						var array_position = $(this).attr('id');
						selected_student (data,array_position);
						$('.dynamic-table-aluno').hide("fast");
						$('.dynamic_form').show("fast");
					});
				}else if(data.length==1){
					selected_student (data,0);
					$('.dynamic_form').show("fast");
				}else if(data.length==0){
					$('.dynamic_message').append(
						'<center><h3>Os dados informados por sua busca não trouxeram nenhum resultado</h3></center><br>'+
						'<center><h4>Por favor verifique os dados e tente novamente</h4></center>'
					);
					$('.dynamic_message').show('fast');
				}		
			}
		});
	};
	

	
	$('.confirm-button').click(function(){
		$(".dynamic_table_body").html("");
		$('.dynamic-table-aluno').hide("fast");
		$('.dynamic_form').hide("fast");
		$('.dynamic_message').html("");
		$('.dynamic_message').hide("fast");
		
		var curso = $('.curso').val();
		var codigo = $('.cod_controle').val();
		var nome = $('.nome').val();
		fill_modal(nome,curso,codigo);
	});
});