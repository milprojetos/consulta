<!DOCTYPE html>
<html lang="en">
	<head>
		<meta name="viewport" 
			content="width=device-width, initial-scale=1, maximum-scale=1, user-scale=no">
		<meta charset="UTF-8">
		<title>Consulta Publica</title>
				
		<!-- scripts header -->
		<?php wp_head(); ?>

	</head>
	<body>
		<div class="container-fluid">
			<div class="page-header pb-5">
				<h1 class="page-title">Consulta Publica de Alunos Concludentes</h1>
			</div>
			
			<div class="row pl-5">
				<div class="col-md-4 border border-secondary rounded" 
					style="background-color:#fef1eb">
					<form method="POST" onsubmit="return false;" id="student_search">
						<div class="form-group" id='curso_in'>
							<label><b>Curso:</b></label>
							<select class="curso curso-data-in form-control" name="curso" >
							</select>
						</div>
						<div class="form-group">
							<label for="funcao"><b>Digite o Nome Completo do Aluno:</b></label><BR>
							<input type="text" id="name_in" name='nome' class="nome form-control" 
								placeholder="Nome Completo do Aluno"/>
						</div>
						<div class="form-group">
						<center>ou use</center>
						</div>
						<!--Segunda Opção de Pesquisa-->
						<div class="form-group">
							<label for="funcao"><b>Numero de Registro:</b></label><BR>
							<input type="number" name='cod_controle' id='cod_reg_in'
							class="cod_controle form-control" 
							placeholder="Digite Aqui o Codigo de Controle do Diploma"/>
						</div>
						<input name='confirm-button' type="hidden" />
						<button data-toggle="modal" class = "confirm-button" 
						data-target=".bd-example-modal-lg" disabled>
							Salvar
						</button> 
						<input type="reset" id='clear_form'/>
					</form>
				</div>

				<div class="col-md-6 pl-5 pt-5">
					<div class="text-center">
						<img src="<?php echo get_bloginfo('template_url') ?>/resources/unice_logo.png"
						class="rounded logo_unice pb-3"/>
					</div>
					<p>Atendendo à Portaria MEC nº 1.095/2018, publicada no Diário Oficial da União em 26.10.2018, que dispõe sobre a expedição e o registro de diplomas de cursos superiores de graduação no âmbito do sistema federal de ensino, a UNICE - Ensino Superior disponibiliza nesta página a consulta publica a todos os diplomas emitidos e autenticados de nossos alunos.</p>
				</div>
			</div>
		</div>
		
		<div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" 
		aria-labelledby="myLargeModalLabel" aria-hidden="true" id='dynamic_large_modal'>
			<div class="modal-dialog modal-lg">
				<div class="modal-content p-3">
					<label><h3><b>Consulta Publica</b></h3></label>
					<div class="dynamic_message" style="display: none;"></div>
					<div class="dynamic-table-aluno" style="display: none;">
						<table class="table">
 							<thead>
								<tr>
									<th scope="col">Codigo de Registro</th>
									<th scope="col">Nome Completo</th>
									<th scope="col">CPF Parcial</th>
									<th scope="col">Curso</th>
								</tr>
							</thead>
							
							<tbody class="dynamic_table_body"></tbody>
						</table>
					</div>
					<form class='dynamic_form' style="display: none;">
						<p><b>Resultado da Consulta:</b></p>
						<div class="form-group row">
							<label for="nome" class="col-sm-1 col-form-label">
								Nome: 
							</label>
							<div class="col-sm-11">
								<input type="text" readonly value="" 
								class="form-control-plaintext" id="nome">
							</div>
						</div>
	
						<div class="form-group row">
							<label for="cpf" class="col-sm-1 col-form-label">
								CPF: 
							</label>
							<div class="col-sm-8">
								<input type="text" readonly class="form-control-plaintext" 
								id="cpf" value="">
							</div>
						</div>
						
						<div class="form-group row">
							<label for="curso" class="col-sm-1 col-form-label">
								Curso: 
							</label>
							<div class="col-sm-5">
								<input type="text" readonly class="form-control-plaintext" 
								id="curso" value="">
							</div>
							<label for="cod_curso" class="col-sm-1 col-form-label">
								Código: 
							</label>
							<div class="col-sm-4">
								<input type="text" readonly class="form-control-plaintext" 
								id="cod_curso" value="">
							</div>
						</div>
	
						<div class="form-group row">
							<label for="ies_exp" class="col-sm-3 col-form-label">
								IES Expedidora: 
							</label>
							<div class="col-sm-5">
								<input type="text" readonly class="form-control-plaintext" 
								id="ies_exp" value="">
							</div>
							<label for="emec_exp" class="col-sm-1 col-form-label">
								EMEC: 
							</label>
							<div class="col-sm-3">
								<input type="text" readonly class="form-control-plaintext" 
								id="emec_exp" value="">
							</div>
						</div>
	
						<div class="form-group row">
							<label for="ies_reg" class="col-sm-3 col-form-label">
								IES Registradora:
							</label>
							<div class="col-sm-5">
								<input type="text" readonly class="form-control-plaintext" 
								id="ies_reg" value="">
							</div>
							<label for="emec_reg" class="col-sm-1 col-form-label">
								EMEC: 
							</label>
							<div class="col-sm-3">
								<input type="text" readonly class="form-control-plaintext" 
								id="emec_reg" value="">
							</div>
						</div>
						
						<div class="form-group row">
							<label for="data_in" class="col-sm-4 col-form-label">
								Data de Ingresso do Aluno: 
							</label>
							<div class="col-sm-2">
								<input type="text" readonly class="form-control-plaintext" 
								id="data_in" value="">
							</div>
							<label for="data_out" class="col-sm-3 col-form-label">
								Data de Conclusão: 
							</label>
							<div class="col-sm-3">
								<input type="text" readonly class="form-control-plaintext" 
								id="data_out" value="">
							</div>
						</div>
						
						<div class="form-group row">
							<label for="data_exp" class="col-sm-4 col-form-label">
								Data de Expedição do Diploma: 
							</label>
							<div class="col-sm-2">
								<input type="text" readonly class="form-control-plaintext" 
								id="data_exp" value="">
							</div>
							<label for="cod_exp" class="col-sm-3 col-form-label">
								Numero de Expedição: 
							</label>
							<div class="col-sm-3">
								<input type="text" readonly class="form-control-plaintext" 
								id="cod_exp" value="">
							</div>
						</div>
						
						<div class="form-group row">
							<label for="data_reg" class="col-sm-4 col-form-label">
								Data de Registro do Diploma: 
							</label>
							<div class="col-sm-2">
								<input type="text" readonly class="form-control-plaintext" 
								id="data_reg" value="">
							</div>
							<label for="cod_reg" class="col-sm-3 col-form-label">
								Numero de Registro: 
							</label>
							<div class="col-sm-3">
								<input type="text" readonly class="form-control-plaintext" 
								id="cod_reg" value="">
							</div>
						</div>
						
						<div class="form-group row">
							<label for="data_dou" class="col-sm-4 col-form-label">
								Data de Pulicação no DOU: 
							</label>
							<div class="col-sm-3">
								<input type="text" readonly class="form-control-plaintext" 
								id="data_dou" value="">
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>

		<!-- scripts footer -->
		<?php wp_footer(); ?>
	</body>
</html>

